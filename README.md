### A CV/Resume Builder web app

1. Setup docker desktop in your machine

2. cd into project directory 
3. cp .env.example .env

Make sure you are not running apache or nginx in your machine. There could be port conflict if you have. Stop apache or nginx before running following command.

4. run `./vendor/bin/sail up -d`

Once the application's Docker containers have been started, you can access the application in your web browser at: http://localhost.

phpmyadmin at http://localhost:8081

DB credentials by default are as follows. You can change it in your .env

username:sail

password:password


5. To migrate and seed, go to docker dashboard and open the cli for the container that is running at port 80 and run `php artisan migrate` & `php artisan db:seed`

**Optional**

If you need to fresh migrate and seed use following command
`php artisan migrate:fresh --seed`

Note: this will drop all tables.
